<?php
return array (
  'multimedia.therealeffingdeal.ru' => 
  array (
    3 => 
    array (
      'url' => 'site/*',
      'app' => 'site',
      'theme' => 'default',
      'theme_mobile' => 'default',
      'locale' => 'ru_RU',
    ),
    0 => 
    array (
      'url' => 'blog/*',
      'app' => 'blog',
      'locale' => 'ru_RU',
      'blog_url_type' => 1,
    ),
    1 => 
    array (
      'url' => 'contacts/*',
      'app' => 'contacts',
      'locale' => 'ru_RU',
      'private' => true,
    ),
    2 => 
    array (
      'url' => '*',
      'app' => 'docflow',
      'module' => 'frontend',
    ),
  ),
);
