<?php /* Smarty version Smarty-3.1.14, created on 2018-12-15 13:53:29
         compiled from "/home/p9670/multimedia.therealeffingdeal.ru/wa-apps/docflow/themes/default/main.html" */ ?>
<?php /*%%SmartyHeaderCode:5667759125bf12bb918d555-76954028%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd5770bd454b1276ee1e1835cce91f484e09b0d29' => 
    array (
      0 => '/home/p9670/multimedia.therealeffingdeal.ru/wa-apps/docflow/themes/default/main.html',
      1 => 1544868471,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5667759125bf12bb918d555-76954028',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5bf12bb91cd235_09596490',
  'variables' => 
  array (
    'contact_data' => 0,
    'wa_static_url' => 0,
    'wa_theme_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bf12bb91cd235_09596490')) {function content_5bf12bb91cd235_09596490($_smarty_tpl) {?><!--<div>main.html</div>-->
<div>
    <h1>Добро пожаловать в систему, <?php echo $_smarty_tpl->tpl_vars['contact_data']->value['login'];?>
</h1>
</div>

<div style="position: relative">
    <div class="preloader">
        <img src="<?php echo $_smarty_tpl->tpl_vars['wa_static_url']->value;?>
wa-apps/docflow/img/preloader128.gif" >
    </div>

    <!--ADMIN-->
    <?php if ($_smarty_tpl->tpl_vars['contact_data']->value['status']=="admin"){?>
        <div class="admin"></div>

    <?php }?>

    <!--MANAGER-->
    <?php if ($_smarty_tpl->tpl_vars['contact_data']->value['status']=="manager"){?>
		<div class="manager"></div>
    <?php }?> 

    <?php if ($_smarty_tpl->tpl_vars['contact_data']->value['status']=="user"){?>
        <div class="user"></div>
    <?php }?>
</div>

<div class="logout">Завершить работу</div>

<script>

    /*B-POPUP*/
    /*MORE*/
    $('html').on('click', ".more_template", function () {
        var attr = $(this).attr("data-id");
        $('.popup-df-template-more').html("");
        $('.popup-df-template-more').bPopup({
            speed: 450,
            transition: 'slideDown',
            transitionClose: 'slideDown'
        });
        $.ajax({
            type: "POST",
            url: '/get_more_template/',
            async: true,
            data: "id=" + attr,
            success: function (data) {
                console.log(data['data']['arr_roles_name'][0]);
                $(".popup-df-template-more").append("<div class='text_name'> Название: "+data['data']['name']+"</div><div class='text_name' style='margin-top: 1em;'>Список ролей</div>");
                for (var i = 0; i < data['data']['arr_roles_name'].length; i++) {
                    $(".popup-df-template-more").append("<div class='box_role_data'><span style='margin-right: 1.5em;'>"+parseInt(i+1)+"</span><span>" + data['data']['arr_roles_name'][i] + "</span></div>");
                }
            }
        });
    })

    $('html').on('click', ".more_route", function () {
        var attr = $(this).attr("data-id");
        $('.popup-df-route-more').html("");
        $('.popup-df-route-more').bPopup({
            speed: 450,
            transition: 'slideDown',
            transitionClose: 'slideDown'
        });
        $.ajax({
            type: "POST",
            url: '/get_more_route/',
            async: true,
            data: "id=" + attr,
            success: function (data) {
                console.log(data);
                $(".popup-df-route-more").append("<div class='text_name'> Название маршрута: "+data['data']['name_route']+"</div>" +
                    "<div class='text_name' style='margin-top: 1em;> Название шаблона: "+data['data']['name_template']+"</div>" +
                    "<div class='text_name' style='margin-top: 1em;'>Список пользователей</div>");
                for (var i = 0; i < data['data']['arr_users_name'].length; i++) {
                    $(".popup-df-route-more").append("<div class='box_role_data'><span style='margin-right: 1.5em;'>"+parseInt(i+1)+"</span><span>" + data['data']['arr_users_name'][i] + "</span></div>");
                }
            }
        });
    })

    /*ADD*/
    $('html').on('click', ".df-add", function () {
        $('.popup-df-content-add-'+$(this).attr("data-tab")).bPopup({
            speed: 450,
            transition: 'slideDown',
            transitionClose: 'slideDown'
        });
        $(".error_message[data-attr='add-"+$(this).attr("data-tab")+"']").html("");
        switch($(this).attr("data-tab")) {
            case "users": {
                $(".choose_roles").html("");
                $.ajax({
                    type: "POST",
                    url: '/get_roles/',
                    async: true,
                    success: function (data) {
                        $(".popup-df-content-add-users .choose_roles").append("<option value='0'>нет</option>");
                        for (var i = 0; i < data['data']['message'].length; i++) {
                            $(".choose_roles").append("<option value='" + data['data']['message'][i]['id'] + "'>" + data['data']['message'][i]['name'] + "</option>");
                        }
                    }
                });
                break;
            }
            case "templates": {
                $('.list_roles_for_templates').html("");
                $.ajax({
                    type: "POST",
                    url: '/get_first_roles/',
                    async: true,
                    success: function (data) {
                        $(".list_roles_for_templates").append("<div class='df-block-choose-roles' data-count='0'>"+
                            "<div style='display: inline-block; margin-right: 1em'>1</div>" +
                            "<select data-count='0' class='choose_roles' style='width: 50%'></select>" +
                            "<img src='<?php echo $_smarty_tpl->tpl_vars['wa_static_url']->value;?>
wa-apps/docflow/img/plus.png' class='df-add-role-to-template' data-count='0'>"+
                            "</div>");
                        $(".list_roles_for_templates .choose_roles").append("<option value='0'>нет</option>");
                        for (var i = 0; i < data['data']['message'].length; i++) {
                            $(".list_roles_for_templates .choose_roles").append("<option value='" + data['data']['message'][i]['id'] + "'>" + data['data']['message'][i]['name'] + "</option>");
                        }
                    }
                    });
                break;
            }
            case "routes":{
                $(".popup-df-content-add-routes .input_name[data-attr='routes_name']").val("");
                $(".popup-df-content-add-routes .list_users").html("");
                $(".popup-df-content-add-routes .choose_template").html("");
                $.ajax({
                    type: "POST",
                    url: '/get_templates/',
                    async: true,
                    success: function (data) {
                        console.log(data);
                        $(".popup-df-content-add-routes .choose_template").append("<option value='0'>нет</option>");
                        for (var i = 0; i < data['data']['message'].length; i++) {
                            $(".choose_template").append("<option value='" + data['data']['message'][i]['id'] + "'>" + data['data']['message'][i]['name'] + "</option>");
                        }
                    }
                });
                break;
            }
        }
    });

    /*FIRST LOAD*/
    var base_attr = "";
    var status = "<?php echo $_smarty_tpl->tpl_vars['contact_data']->value['status'];?>
";
    if(status == "admin") {
		$(".admin").load("<?php echo $_smarty_tpl->tpl_vars['wa_theme_url']->value;?>
admin.html"); 
        base_attr = "roles";
    }
    else
    if(status == "manager")
    {
		$(".manager").load("<?php echo $_smarty_tpl->tpl_vars['wa_theme_url']->value;?>
manager.html"); 
        base_attr = "users";
    }
    if(status == "user")
    {
		$(".user").load("<?php echo $_smarty_tpl->tpl_vars['wa_theme_url']->value;?>
user.html"); 
        base_attr = "add-docs";
    }
    loadData(base_attr);

</script><?php }} ?>