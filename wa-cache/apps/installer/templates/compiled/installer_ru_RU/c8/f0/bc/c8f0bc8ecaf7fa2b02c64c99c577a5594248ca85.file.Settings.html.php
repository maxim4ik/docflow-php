<?php /* Smarty version Smarty-3.1.14, created on 2018-11-21 09:59:46
         compiled from "/home/p9670/multimedia.therealeffingdeal.ru/wa-apps/installer/templates/actions/settings/Settings.html" */ ?>
<?php /*%%SmartyHeaderCode:7120600115bf502627c3537-28782459%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c8f0bc8ecaf7fa2b02c64c99c577a5594248ca85' => 
    array (
      0 => '/home/p9670/multimedia.therealeffingdeal.ru/wa-apps/installer/templates/actions/settings/Settings.html',
      1 => 1532534011,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7120600115bf502627c3537-28782459',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'site_app_exists' => 0,
    'wa_backend_url' => 0,
    'system_settings_url' => 0,
    'version' => 0,
    'wa_app_static_url' => 0,
    'wa' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5bf502638d8600_68119971',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bf502638d8600_68119971')) {function content_5bf502638d8600_68119971($_smarty_tpl) {?><div class="shadowed">
    <div class="i-white-core-content">
        <div class="block double-padded">
            <div class="fields">
                <div class="field-group">
                    <div class="field">
                        <div class="name">
                            Системные настройки
                        </div>
                        <div class="value">
                            <?php if (!$_smarty_tpl->tpl_vars['site_app_exists']->value){?>
                                <em><?php echo sprintf('Установите <a href="%sinstaller/#/apps/site/">приложение «Сайт»</a> для управления системныни настройками.',$_smarty_tpl->tpl_vars['wa_backend_url']->value);?>
</em>
                            <?php }else{ ?>
                                <?php echo sprintf('<a href="%s">Управляйте системными настройками в приложении «Сайт»</a>.',$_smarty_tpl->tpl_vars['system_settings_url']->value);?>

                            <?php }?>
                        </div>
                    </div>
                    <div class="field">
                        <div class="name">
                            Версия Вебасиста
                        </div>
                        <div class="value no-shift"><?php echo $_smarty_tpl->tpl_vars['version']->value;?>
</div>
                    </div>
                </div>
                <div class="field-group">
                    <div class="field">
                        <div class="name" style="margin-top: 8px;">
                            Очистить кеш
                        </div>
                        <div class="value">
                            <input type="button" class="button gray" name="clear_cache" value="Очистить кеш">
                            <span id="installer-cache-state" style="display: none;"><!-- state placeholder --></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>

<script src="<?php echo $_smarty_tpl->tpl_vars['wa_app_static_url']->value;?>
js/settings.js?v=<?php echo $_smarty_tpl->tpl_vars['wa']->value->version();?>
" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $.installer_settings.init();
    });
</script><?php }} ?>