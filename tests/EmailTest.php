<?php
declare(strict_types=1);

//include '/home/p9670/multimedia.therealeffingdeal.ru/wa-apps/docflow/lib/actions/frontend/docflowFrontendLogin.action.php';
//include '/home/p9670/multimedia.therealeffingdeal.ru/src/Email.php';
include '/root/docflow/src/Email.php';
use PHPUnit\Framework\TestCase;

final class EmailTest extends TestCase
{
    public function testCanBeCreatedFromValidEmailAddress(): void
    {
        $this->assertInstanceOf(
            Email::class,
            Email::fromString('user@example.com')
        );
    }

    public function testCannotBeCreatedFromInvalidEmailAddress(): void
    {
        $this->expectException(InvalidArgumentException::class);

        Email::fromString('invalid');
    }

    public function testCanBeUsedAsString(): void
    {
        $this->assertEquals(
            'user@example.com',
            Email::fromString('user@example.com')
        );
    }

   /* public function testLogin():void
    {
        $this->assertInstanceOf(
            docflowFrontendAdminAddTemplatesController::class,
            docflowFrontendAdminAddTemplatesController::execute()
        );
    }*/
}