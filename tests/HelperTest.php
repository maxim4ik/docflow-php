<?php
declare(strict_types=1);

//include '/home/p9670/multimedia.therealeffingdeal.ru/wa-apps/docflow/lib/actions/frontend/Helper.php';
include '/root/docflow/wa-apps/docflow/lib/actions/frontend/Helper.php';
use PHPUnit\Framework\TestCase;

final class HelperTest extends TestCase
{

    public function testCheckIdTrue(): void{
        $helper = new Helper();
        $this->assertTrue(
            $helper->checkID(1)
        );
    }

    public function testCheckIdFalse(): void{
        $helper = new Helper();
        $this->assertFalse(
            $helper->checkID("1")
        );
    }

    public function checkEmptyLogin():void{
        $helper = new Helper();
        $this->assertFalse(
            $helper->checkEmptyLoginOrPassword("", "password")
        );
    }

    public function checkEmptyPassword():void{
        $helper = new Helper();
        $this->assertFalse(
            $helper->checkEmptyLoginOrPassword("login", "")
        );
    }
}