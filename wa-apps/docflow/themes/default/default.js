/*ADD ROLE TO TEMPLATE*/
$('html').on('click', ".df-add-role-to-template",function () {
    var attr_count = $(this).attr("data-count");
	console.log("attr_count: "+attr_count);
    var select_id = $(".list_roles_for_templates .choose_roles[data-count='"+attr_count+"'] :selected").val();
    if(select_id != 0) {
        $.ajax({
            type: "POST",
            url: '/get_roles_excetp_id/',
            async: true,
            data: "id=" + select_id,
            success: function (data) {
                $(".df-add-role-to-template[data-count='"+attr_count+"']").hide();
                console.log(data['data']);
                var temp_one = parseInt(attr_count)+1;
                var temp_two = parseInt(attr_count)+2;
                $(".list_roles_for_templates").append("<div class='df-block-choose-roles' data-count='"+temp_one+"'>"+
                    "<div style='display: inline-block; margin-right: 1em'>"+temp_two+"</div>" +
                    "<select data-count='"+temp_one+"' class='choose_roles' style='width: 50%'></select>" +
                    "<img src='/wa-apps/docflow/img/plus.png' class='df-add-role-to-template' data-count='"+temp_one+"'>"+
                    "</div>");
                $(".list_roles_for_templates .choose_roles").append("<option value='0'>нет</option>");
                for (var i = 0; i < data['data']['message'].length; i++) {
                    $(".list_roles_for_templates .choose_roles").append("<option value='" + data['data']['message'][i]['id'] + "'>" + data['data']['message'][i]['name'] + "</option>");
                }
            }
        });
    }
    else
    {
        $(".error_message[data-attr='add-templates']").html("Роль не может быть пустой!");
    }
});

$('html').on('change', ".list_roles_for_templates .choose_roles", function () {
    if($(".error_message[data-attr='add-templates']").html() != "") {
        $(".error_message[data-attr='add-templates']").html("");
    }
    var data_count = $(this).attr("data-count");
    $(".df-add-role-to-template[data-count='"+data_count+"']").show();
    $(".df-block-choose-roles").each(function () {
        console.log($(this).attr("data-count"));
        if(parseInt($(this).attr("data-count")) > parseInt(data_count))
        {
            console.log("this: "+$(this).attr("data-count")+" change: "+data_count);
            $(".df-block-choose-roles[data-count='"+$(this).attr("data-count")+"']").remove();
        }
    });
});

/*ADD ROUTES*/
$('html').on('change', ".popup-df-content-add-routes .choose_template", function () {
    if($(".error_message[data-attr='add-routes']").html() != "") {
        $(".error_message[data-attr='add-routes']").html("");
    }

    $(".popup-df-content-add-routes .list_users").html("");

    var template_id = $(this).val();
    if(template_id != "0")
    {
        $.ajax({
            type: "POST",
            url: '/get_templates_users/',
            async: true,
            data: "id=" + template_id,
            success: function (data) {
                console.log(data['data']['arr_users'][0][0]);
                $(".popup-df-content-add-routes .list_users").append("<div class='text_name' style='margin-top: 1em;'>Пользователи</div>");
                for(var i=0; i < data['data']['arr_users'].length; i++)
                {

                    $(".popup-df-content-add-routes .list_users").append("<div style='margin-top: 1em;'>" +
                                                                                "<select class='choose_user choose_user_"+i+"'>" +
                                                                                "<option value='0'>нет</option>" +
                                                                            "</select>" +
                                                                         "</div>");
                    for(var ii=0; ii < data['data']['arr_users'][i].length; ii++) {
                        $(".choose_user_"+i).append("<option value='"+data['data']['arr_users'][i][ii]['id']+"'>"+data['data']['arr_users'][i][ii]['login']+"</option>");
                    }
                }
            }
        });
    }
});

/*ADD BUTTON*/

$('html').on('click', ".enter_form", function() {
    var attr = $(this).attr("data-attr");

    switch (attr) {
        case "roles": {
            var read_roles = $("input[data-attr='read_roles']").prop("checked");
            var edit_roles = $("input[data-attr='edit_roles']").prop("checked");
            var add_roles = $("input[data-attr='add_roles']").prop("checked");
            var name = $(".input_name[data-attr='roles']").val();

            $.ajax({
                type: "POST",
                url: '/add_roles/',
                async: true,
                data: "name=" + name + "&read_roles=" + read_roles + "&edit_roles=" + edit_roles + "&add_roles=" + add_roles,
                success: function (data) {
                    console.log(data['data']);
                    if (data['data']['result'] == 0) {
                        $(".error_message[data-attr='add-roles']").html(data['data']['message']);
                    }
                    else {
                        $('.b-close').trigger('click');
                        $(".input_name[data-attr='roles']").val("");
                        $("input[data-attr='read_roles']").prop('checked', false);
                        $("input[data-attr='edit_roles']").prop('checked', false);
                        $("input[data-attr='add_roles']").prop('checked', false);
                        loadData("roles");
                    }
                }
            });
            break;
        }

        case "users":{
            var login = $(".input_name[data-attr='login']").val();
            var password = $(".input_name[data-attr='password']").val();
            var role_id = $(".popup-df-content-add-users .choose_roles :selected").val();

            $.ajax({
                type: "POST",
                url: '/add_users/',
                async: true,
                data: "login=" + login + "&password=" + password + "&role_id=" + role_id,
                success: function (data) {
                    console.log(data['data']);
                    if (data['data']['result'] == 0) {
                        $(".error_message[data-attr='add-users']").html(data['data']['message']);
                    }
                    else {
                        $('.b-close').trigger('click');
                        $(".input_name[data-attr='login']").val("");
                        $(".input_name[data-attr='password']").val("");
                        loadData("users");
                    }
                }
            });
            break;
        }

        case "templates":{
            var name = $(".input_name[data-attr='templates_name']").val();
            var arr_id = [];
            $(".df-block-choose-roles").each(function () {
                //arr_id["'"+$(this).attr("data-count")+"'"]=$(this).attr("data-count");
                arr_id.push($(".list_roles_for_templates .choose_roles[data-count='"+$(this).attr("data-count")+"'] :selected").val());
            });
            $.ajax({
                type: "POST",
                url: '/add_templates/',
                async: true,
                dataType: 'json',
                data: { name : name, arr_id: arr_id },
                success: function (data) {
                    console.log(data['data']);
                    if (data['data']['result'] == 0) {
                        $(".error_message[data-attr='add-templates']").html(data['data']['message']);
                    }
                    else {
                        $('.b-close').trigger('click');
                        $(".input_name[data-attr='templates_name']").val("");
                        loadData("templates");
                    }
                }
            });
            break;
        }

        case "routes": {
            var name = $(".popup-df-content-add-routes .input_name[data-attr='routes_name']").val();
            var template_id = $(".popup-df-content-add-routes .choose_template :selected").val();
            console.log("name: "+name);
            var arr_users_id = [];
            $(".popup-df-content-add-routes .list_users select").each(function () {
                arr_users_id.push($(this).val())
            });
            $.ajax({
                type: "POST",
                url: '/add_route/',
                async: true,
                dataType: 'json',
                data: { name : name, template_id: template_id, arr_users_id: arr_users_id },
                success: function (data) {
                    console.log(data['data']);
                    if (data['data']['result'] == 0) {
                        $(".error_message[data-attr='add-routes']").html(data['data']['message']);
                    }
                    else {
                        $('.b-close').trigger('click');
                        $(".input_name[data-attr='templates_name']").val("");
                        loadData("routes");
                    }
                }
            });
            break;
        }

    }
});

/*TABS*/
$('html').on('click', ".df-tabs li", function () {
    var attr = $(this).attr("data-tab");
    if(!$(this).hasClass("selected"))
    {
        $(".df-tabs li").removeClass("selected");
        $(this).addClass("selected");
        loadData(attr);
    }
});

/*LOAD CONTENT*/
function loadData(attr) {
    $(".preloader").show();


    $(".df-tab-content").hide();
    $(".df-content-all-"+attr).html("");
    $(".df-tab-content[data-tab='"+attr+"']").show();

    $.ajax({
        type: "POST",
        url: '/get_'+attr+'/',
        async: true,
        success: function(data){
            console.log(data['data']);
            printData(attr, data['data']);
        }
    });
}

function printData(attr, data) {
    switch (attr)
    {
        case "roles":
        {
            if(data['message'].length == 0)
            {
                $(".df-content-all-"+attr).append("<div style='text-align: center; font-size: 23px'>Записи отсутствуют</div>");
            }
            else
            {
                $(".df-content-all-"+attr).append("<div>" +
                        "<div class='table_roles'>НАЗВАНИЕ</div>" +
                        "<div class='table_roles'>ПРОСМОТР</div>" +
                        "<div class='table_roles'>РЕДАКТИРОВАНИЕ</div>" +
                        "<div class='table_roles'>СОЗДАНИЕ</div>" +
                    "</div>");
                for(var i=0; i < data['message'].length; i++)
                {
                    var cl = "table_roles_"+i;
                    $(".df-content-all-"+attr).append("<div class='table_roles'>"+data['message'][i]['name']+"</div>");
                    if(data['message'][i]['status_read'] == "true")
                    {
                        $(".df-content-all-"+attr).append("<div  class='table_roles'>&#9989</div>");
                    }else{
                        $(".df-content-all-"+attr).append("<div  class='table_roles'>&#8722</div>");
                    }
                    if(data['message'][i]['status_edit'] == "true")
                    {
                        $(".df-content-all-"+attr).append("<div  class='table_roles'>&#9989</div>");
                    }else{
                        $(".df-content-all-"+attr).append("<div  class='table_roles'>&#8722</div>");
                    }
                    if(data['message'][i]['status_add'] == "true")
                    {
                        $(".df-content-all-"+attr).append("<div  class='table_roles'>&#9989</div>");
                    }else{
                        $(".df-content-all-"+attr).append("<div  class='table_roles'>&#8722</div>");
                    }

                }
            }
            break;
        }
        case "templates":
        {
            if(data['message'].length == 0)
            {
                $(".df-content-all-"+attr).append("<div style='text-align: center; font-size: 23px'>Записи отсутствуют</div>");
            }
            else
            {
                for(var i=0; i < data['message'].length; i++)
                {
                    $(".df-content-all-"+attr).append("<div>" +
                            "<div class='table_templates'>" +
                                "<div class='table_template'>"+data['message'][i]['name']+"</div>" +
                                "<div class='table_template'>" +
                                    "<span class='more_template' data-id='"+data['message'][i]['id']+"'>ПОДРОБНЕЕ</span>" +
                                "</div>" +
                            "</div>" +
                        "</div>");
                }
            }
            break;
        }
        case "users":
        {
            if(data['message'].length == 0)
            {
                $(".df-content-all-"+attr).append("<div style='text-align: center; font-size: 23px'>Записи отсутствуют</div>");
            }
            else
            {
                $(".df-content-all-"+attr).append("<div>" +
                    "<div class='table_roles'>ЛОГИН</div>" +
                    "<div class='table_roles'>РОЛЬ</div>" +
                    "</div>");
                for(var i=0; i < data['message'].length; i++)
                {
                    $(".df-content-all-"+attr).append("<div>" +
                        "<div class='table_roles'>"+data['message'][i]['login']+"</div>" +
                        "<div class='table_roles'>"+data['message'][i]['role']+"</div>" +
                        "</div>");
                }
            }
            break;
        }
        case "routes":
        {
            if(data['message'].length == 0)
            {
                $(".df-content-all-"+attr).append("<div style='text-align: center; font-size: 23px'>Записи отсутствуют</div>");
            }
            else
            {
                for(var i=0; i < data['message'].length; i++)
                {
                    $(".df-content-all-"+attr).append("<div>" +
                        "<div class='table_routes'>" +
                        "<div class='table_route'>"+data['message'][i]['name']+"</div>" +
                        "<div class='table_route'>" +
                        "<span class='more_route' data-id='"+data['message'][i]['id']+"'>ПОДРОБНЕЕ</span>" +
                        "</div>" +
                        "</div>" +
                        "</div>");
                }
            }
        }
    }
    $(".preloader").hide();
}

/*LOGOUT*/
$('html').on('click', ".logout", function () {
    $.ajax({
        type: "POST",
        url: '/logout/',
        async: true,
        success: function(data){
            window.location.replace("/login/");
        }
    });
});