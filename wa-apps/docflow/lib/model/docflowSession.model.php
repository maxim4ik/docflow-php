<?php

class docflowSessionModel extends waModel
{
    protected $id = 'session_code';
    protected $table = 'docflow_session';

    public function createSession($contact_id)
    {
        $code = $this->generateCode();
        $this->insert(array(
            'session_code' => $code,
            'contact_id' => $contact_id
        ));

        //$session_users_model = new terminalSessionUsersModel();
        //$session_users_model->addUser($code, $contact_id, 1);

        return $code;
    }

   /* public function clearByCode($code)
    {
        $this->query("DELETE FROM ".$this->table." WHERE code = s:code", array('code' => $code));
        $users_model = new terminalSessionUsersModel();
        $users_model->deleteByCode($code);
        $items_model = new terminalCartItemsModel();
        $items_model->deleteByCode($code);
        $cart_params_model = new terminalCartParamsModel();
        $cart_params_model->deleteById($code);
    }*/

    protected function generateCode()
    {
        $code = $this->query("SELECT UUID() AS uuid")->fetchAll();
        return $code[0]['uuid'];
    }

    public function getContactID($session_code)
    {
        return $this->query("SELECT * FROM  ".$this->table." WHERE session_code = s:session_code", array('session_code' => $session_code))->fetchAll();
    }

}
