<?php

class docflowRoutesUsersModel extends waModel
{
//    protected $id = 'session_code';
    protected $table = 'docflow_routes_users';

    public function addRoutesUsers($arr)
    {
        $this->multipleInsert($arr);
    }

    public function getUsersID($id)
    {
        return $this->query("SELECT * FROM " . $this->table . " WHERE id_route = i:id", array('id' => $id))->fetchAll();
    }

}
