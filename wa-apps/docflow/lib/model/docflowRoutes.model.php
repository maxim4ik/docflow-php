<?php

class docflowRoutesModel extends waModel
{
    protected $table = 'docflow_routes';

    public function getRoutes()
    {
        return $this->query("SELECT * FROM " . $this->table )->fetchAll();
    }

    public function getCountRoute($name)
    {
        return $this->query("SELECT * FROM " . $this->table . " WHERE name = s:name", array('name' => $name))->fetchAll();
    }

    public function addRoute($name, $template_id)
    {
        $this->insert(array(
            'name' => $name,
            'template_id' => $template_id
        ));
    }

    public function getRouteByName($name)
    {
        return $this->query("SELECT * FROM " . $this->table . " WHERE name = s:name", array('name' => $name))->fetchAll();
    }

    public function getRouteID($id)
    {
        return $this->query("SELECT * FROM " . $this->table . " WHERE id = s:id", array('id' => $id))->fetchAll();
    }

}