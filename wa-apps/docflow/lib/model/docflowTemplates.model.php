<?php

class docflowTemplatesModel extends waModel
{
//    protected $id = 'session_code';
    protected $table = 'docflow_templates';

    public function getTemplates()
    {
        return $this->query("SELECT * FROM " . $this->table )->fetchAll();
    }

    public function getTemplate($name)
    {
        return $this->query("SELECT * FROM " . $this->table . " WHERE name = s:name", array('name' => $name))->fetchAll();
    }

    public function countTemplate($name)
    {
        return $this->query("SELECT * FROM " . $this->table . " WHERE name = s:name", array('name' => $name))->count();
    }

    public function addTemplate($name)
    {
        //$this->query("INSERT * FROM ".$this->table." WHERE  name = s:name", array('name' => $name))
        $this->insert(array(
            'name' => $name
        ));
    }

    public function getTemplateID($id)
    {
        return $this->query("SELECT * FROM " . $this->table . " WHERE id = s:id", array('id' => $id))->fetchAll();
    }

}