<?php

class docflowTemplatesRolesModel extends waModel
{
//    protected $id = 'session_code';
    protected $table = 'docflow_templates_roles';

    public function addTemplatesRoles($arr_id)
    {
        //$this->query("INSERT * FROM ".$this->table." WHERE  name = s:name", array('name' => $name))
        $this->multipleInsert($arr_id);
    }

    public function getRolesID($id)
    {
        return $this->query("SELECT * FROM " . $this->table . " WHERE id_template = i:id", array('id' => $id))->fetchAll();
    }

}
