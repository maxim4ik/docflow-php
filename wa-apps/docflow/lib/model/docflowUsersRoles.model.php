<?php

class docflowUsersRolesModel extends waModel
{
//    protected $id = 'session_code';
    protected $table = 'docflow_roles';

    public function getRoles()
    {
        return $this->query("SELECT * FROM  ".$this->table)->fetchAll();
    }

    public function countRoles($name)
    {
        return $this->query("SELECT * FROM ".$this->table." WHERE  name = s:name", array('name' => $name))->count();
    }

    public function getFirstRoles()
    {
        return $this->query("SELECT * FROM ".$this->table." WHERE status_add = 'true'")->fetchAll();
    }

    public function getRolesExeptedId($id)
    {
        return $this->query("SELECT * FROM ".$this->table." WHERE id NOT LIKE i:id", array('id' => $id))->fetchAll();
    }

    public function addRoles($name, $status_read, $status_edit, $status_add)
    {
        //$this->query("INSERT * FROM ".$this->table." WHERE  name = s:name", array('name' => $name))
        $this->insert(array(
            'name' => $name,
            'status_read' => $status_read,
            'status_edit' => $status_edit,
            'status_add' => $status_add,
        ));
    }

    public function getRole($id)
    {
        return $this->query("SELECT * FROM ".$this->table." WHERE id = i:id", array('id' => $id))->fetchAll();
    }

}
