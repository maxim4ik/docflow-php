<?php

class docflowContactModel extends waModel
{
//    protected $id = 'session_code';
    protected $table = 'docflow_contact';

    public function getContact($login, $password)
    {
        return $this->query("SELECT * FROM ".$this->table." WHERE login = s:login AND password = s:password", array('login' => $login, 'password' => $password))->fetchAll();
    }

    public function getContactData($contact_id)
    {
        return $this->query("SELECT * FROM ".$this->table." WHERE id = i:id", array('id' => $contact_id))->fetchAll();
    }

    public function getUsersContact()
    {
        //$roles_model = new docflowUsersRolesModel();
        return $this->query("SELECT dc.login, dr.name AS role
                                FROM ".$this->table." AS dc
                                LEFT JOIN (SELECT * FROM docflow_roles) AS dr ON dc.roles_id = dr.id
                                WHERE status = 'user'")->fetchAll();
    }

    public function countUser($login)
    {
        return $this->query("SELECT * FROM ".$this->table." WHERE login = s:login", array('login' => $login))->count();
    }

    public function addUser($login, $password, $role_id)
    {
        //$this->query("INSERT * FROM ".$this->table." WHERE  name = s:name", array('name' => $name))
        $this->insert(array(
            'login' => $login,
            'password' => $password,
            'status' => 'user',
            'roles_id' => $role_id,
        ));
    }

    public function getTemaplatesContacts($id)
    {
        return $this->query("SELECT * FROM ".$this->table." WHERE roles_id = i:id", array('id' => $id))->fetchAll();
    }

    public function getContactID($id)
    {
        return $this->query("SELECT * FROM ".$this->table." WHERE id = i:id", array('id' => $id))->fetchAll();
    }

    /*    public function createSession($contact_id, $shop_id)
        {
            $code = $this->generateCode();
            $this->insert(array(
                'code' => $code,
                'shop_id' => $shop_id,
            ));

            $session_users_model = new terminalSessionUsersModel();
            $session_users_model->addUser($code, $contact_id, 1);

            return $code;
        }

        public function clearByCode($code)
        {
            $this->query("DELETE FROM ".$this->table." WHERE code = s:code", array('code' => $code));
            $users_model = new terminalSessionUsersModel();
            $users_model->deleteByCode($code);
            $items_model = new terminalCartItemsModel();
            $items_model->deleteByCode($code);
            $cart_params_model = new terminalCartParamsModel();
            $cart_params_model->deleteById($code);
        }

        protected function generateCode()
        {
            $code = $this->query("SELECT UUID() AS uuid")->fetchAll();
            return $code[0]['uuid'];
        }*/
}
