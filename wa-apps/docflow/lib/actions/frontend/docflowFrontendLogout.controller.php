<?php

class docflowFrontendLogoutController extends waJsonController
{
    public function execute()
    {
        $session = new docflowSession();
        if(!$session->isAuth()) {wa()->getResponse()->redirect(wa()->getRouteUrl('/frontend/login'), 302); return;}

        $session->logout();
        $this->response = array('result' => 1, 'message' => 'Вы завершили работу!');

        //		wa()->getResponse()->redirect(wa()->getRouteUrl('terminal/frontend/login'), 302);
    }
}