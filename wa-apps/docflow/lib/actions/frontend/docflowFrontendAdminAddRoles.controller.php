<?php

class docflowFrontendAdminAddRolesController extends waJsonController
{
    public function execute()
    {

        $name = waRequest::post('name', null);
        if(empty($name))
        {
            $this->response = array('result' => 0, 'message' => "Введите название роли");
            return;
        }
        $read_roles = waRequest::post('read_roles', null);
        $edit_roles = waRequest::post('edit_roles', null);
        $add_roles = waRequest::post('add_roles', null);

        if($read_roles == "false" && $edit_roles  == "false" && $add_roles == "false")
        {
            $this->response = array('result' => 0, 'message' => "Выберите права для роли");
            return;
        }

        $roles_model = new docflowUsersRolesModel();
        if($roles_model->countRoles($name))
        {
            $this->response = array('result' => 0, 'message' => "Данное название уже существует");
            return;
        }

        $roles_model->addRoles($name, $read_roles, $edit_roles, $add_roles);
        $this->response = array('result' => 1);
        //$this->response = array('result' => 1, 'message' => $roles_data);
    }
}