<?php

class docflowFrontendAndroidManagerGetRoutesController extends waJsonController
{
    public function execute()
    {
        $routes_model = new docflowRoutesModel();
        $routes_data = $routes_model->getRoutes();
        $this->response = array('result' => 1, 'message' => $routes_data);
    }
}