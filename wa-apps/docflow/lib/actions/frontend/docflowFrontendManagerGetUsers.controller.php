<?php

class docflowFrontendManagerGetUsersController extends waJsonController
{
    public function execute()
    {
        $contact_model = new docflowContactModel();
        $users_data = $contact_model->getUsersContact();
        $this->response = array('result' => 1, 'message' => $users_data);
    }
}