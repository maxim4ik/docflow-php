<?php
declare(strict_types=1);
//include '/home/p9670/multimedia.therealeffingdeal.ru/src/Email.php';
//include './Helper.php';

class Helper
{
    public static function checkEmptyLoginOrPassword($login, $password){
        if(empty($login) || empty($password))
            return false;
        return true;
    }

    public static function checkID($id){
        return is_int($id);
    }

    public static function checkInt($id){
        return (int) $id;
    }

}