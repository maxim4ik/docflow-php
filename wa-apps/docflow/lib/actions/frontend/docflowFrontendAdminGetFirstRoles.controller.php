<?php

class docflowFrontendAdminGetFirstRolesController extends waJsonController
{
    public function execute()
    {
        $roles_model = new docflowUsersRolesModel();
        $roles_data = $roles_model->getFirstRoles();
        $this->response = array('result' => 1, 'message' => $roles_data);
    }
}