<?php
declare(strict_types=1);
class docflowFrontendAdminAddTemplatesController extends waJsonController
{
    public function execute()
    {

        $name = waRequest::post('name', null);
        if(empty($name))
        {
            $this->response = array('result' => 0, 'message' => "Введите название шаблона");
            return;
        }

        $data_arr_id = array();
        $arr_id = waRequest::post('arr_id', null);
        foreach ($arr_id as $a)
        {
            if($a == 0)
            {
                $this->response = array('result' => 0, 'message' => "Роль не может быть пустой!");
                return;
            }
//            file_put_contents('$arr_id.txt', $a."\n", FILE_APPEND);
        }

        $templates_model = new docflowTemplatesModel();
        if($templates_model->countTemplate($name))
        {
            $this->response = array('result' => 0, 'message' => "Данное название уже существует");
            return;
        }

        $templates_model->addTemplate($name);
        $template_data = $templates_model->getTemplate($name);

        foreach ($arr_id as $a)
        {
            array_push($data_arr_id, array('id_template' => $template_data[0][id] , 'id_role' => $a));
        }

        $templates_roles_model = new docflowTemplatesRolesModel();
        $templates_roles_model->addTemplatesRoles($data_arr_id);
//        file_put_contents('$template_data.txt', , FILE_APPEND);

        $this->response = array('result' => 1);
        //$this->response = array('result' => 1, 'message' => $roles_data);
    }
}