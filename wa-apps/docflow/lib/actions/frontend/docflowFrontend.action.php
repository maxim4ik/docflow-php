<?php

class docflowFrontendAction extends waViewAction
{
    public function execute()
    {
        $session = new docflowSession();
        if(!$session->isAuth()) {wa()->getResponse()->redirect(wa()->getRouteUrl('/frontend/login'), 302); return;}

        $this->setLayout(new docflowFrontendLayout());
        $this->setThemeTemplate('main.html');

        $session_code = waRequest::cookie('sessid', null, 'string');
        $session_model = new docflowSessionModel();
        $session_data = $session_model->getContactID($session_code);
        $contact_model = new docflowContactModel();
        $contact_data = $contact_model->getContactData($session_data[0]['contact_id']);
        $this->view->assign('contact_data', $contact_data[0]);
    }
}