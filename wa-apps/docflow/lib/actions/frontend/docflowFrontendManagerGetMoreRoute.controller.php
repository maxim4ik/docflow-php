<?php

class docflowFrontendManagerGetMoreRouteController extends waJsonController
{
    public function execute()
    {
        $id = waRequest::post('id', null);
        $routes_model = new docflowRoutesModel();
        $route_data = $routes_model->getRouteID($id);

        $templates_model = new docflowTemplatesModel();
        $template_data = $templates_model->getTemplateID($route_data[0]['template_id']);

        $routes_users_model = new docflowRoutesUsersModel();
        $users_id = $routes_users_model->getUsersID($id);
        $arr_users= array();

        $contact_model = new docflowContactModel();


        foreach ($users_id as $ui)
        {
            $data_contact = $contact_model->getContactID($ui['id_user']);
            array_push($arr_users, $data_contact[0]['login']);
        }

        $this->response = array('name_route' => $route_data[0]['name'], 'name_template' => $template_data[0]['name'], 'arr_users_name' => $arr_users);
    }
}