<?php

class docflowFrontendAdminGetRolesExceptedIdController extends waJsonController
{
    public function execute()
    {
        $id = waRequest::post('id', null);
        $roles_model = new docflowUsersRolesModel();
        $roles_data = $roles_model->getRolesExeptedId($id);
        $this->response = array('result' => 1, 'message' => $roles_data);
    }
}