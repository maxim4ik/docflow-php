<?php

class docflowFrontendAuthController extends waJsonController
{
    public function execute()
    {
        $session = new docflowSession();
        if($session->isAuth()) {wa()->getResponse()->redirect(wa()->getRouteUrl('/frontend'), 302); return;}


        $login_data = waRequest::post('login', null);
        $password_data = waRequest::post('password', null);
        if(empty($login_data) || empty($password_data))
        {
            $this->response = array('result' => 0, 'message' => 'Не все поля заполнены!');
            return;
        }

        $contact_model = new docflowContactModel();
        $contact_data = $contact_model->getContact($login_data, $password_data);
        //file_put_contents('docflowFrontendAuthController.txt',print_r($contact_data), FILE_APPEND);
        if(empty($contact_data))
        {
            $this->response = array('result' => 0, 'message' => "Неверный логин или пароль!");
            return;
        }


        $session->initSession($contact_data[0]['id']);
        $this->response = array('result' => 1, 'message' => "успешно");


/*        if(!$session->isAuth()) {wa()->getResponse()->redirect(wa()->getRouteUrl('/frontend/login'), 302); return;}
       */
        //        echo "docflowFrontendAuthController";
/*        $session = new docflowSession();
        if($session->isAuth()) {wa()->getResponse()->redirect(wa()->getRouteUrl('/frontend'), 302); return;}


        $this->setLayout(new docflowFrontendLayout());
        $this->setThemeTemplate('login.html');

        $this->view->assign('error', "errrooooorrrr");*/
        /*  $frontend_helper = new bagginsFrontendHelper();
          $rights_error = $frontend_helper->checkRights();
          if($rights_error) {$this->response = array('error' => $rights_error); return;}

          $query = waRequest::get('q', '', 'string');
          $product_id = waRequest::get('product_id', 0, 'int');
          $addition_model = new bagginsAdditionModel();
          $additions = $addition_model->search($query, $product_id);
          if(!count($additions)) {$this->response = array(); return;}

          $result = array();
          foreach($additions as $key => $addition)
          {
              array_push($result, array('id' => $key, 'text' => htmlspecialchars($addition['name'], ENT_QUOTES)));
          }
          $this->response = $result;*/
    }
}