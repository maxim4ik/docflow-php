<?php

class docflowFrontendAdminGetRolesController extends waJsonController
{
    public function execute()
    {
        $roles_model = new docflowUsersRolesModel();
        $roles_data = $roles_model->getRoles();
        $this->response = array('result' => 1, 'message' => $roles_data);
    }
}