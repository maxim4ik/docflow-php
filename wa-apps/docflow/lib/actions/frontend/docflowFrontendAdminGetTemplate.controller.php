<?php

include '/home/p9670/multimedia.therealeffingdeal.ru/wa-apps/docflow/lib/actions/frontend/Helper.php';

class docflowFrontendAdminGetTemplateController extends waJsonController
{
    public function execute()
    {
        $helper = new Helper();
        $id = waRequest::post('id', null);
        if(!$helper->checkID($id))
            return;
        $id = $helper->checkInt($id);
        $templates_model = new docflowTemplatesModel();
        $template_data = $templates_model->getTemplateID($id);

        $templates_roles_model = new docflowTemplatesRolesModel();
        $template_roles_id = $templates_roles_model->getRolesID($id);
        $arr_roles = array();

        $user_roles_model = new docflowUsersRolesModel();


        foreach ($template_roles_id as $tri)
        {
            $data_role = $user_roles_model->getRole($tri['id_role']);
            array_push($arr_roles, $data_role[0]['name']);
        }

        $this->response = array('name' => $template_data[0]['name'], 'arr_roles_name' => $arr_roles);
    }
}