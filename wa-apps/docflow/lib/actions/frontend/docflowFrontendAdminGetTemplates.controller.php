<?php

class docflowFrontendAdminGetTemplatesController extends waJsonController
{
    public function execute()
    {
        $templates_model = new docflowTemplatesModel();
        $templates_data = $templates_model->getTemplates();
        $this->response = array('result' => 1, 'message' => $templates_data);
    }
}