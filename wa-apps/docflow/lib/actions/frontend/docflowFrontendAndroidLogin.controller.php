<?php

include '/home/p9670/multimedia.therealeffingdeal.ru/wa-apps/docflow/lib/actions/frontend/Helper.php';

class docflowFrontendAndroidLoginController extends waJsonController
{
    public function execute()
    {
        $helper = new Helper();
        $login_data = waRequest::get('login', null);
        $password_data = waRequest::get('password', null);
/*        $this->response = array('result' => 0, 'message' => $_POST);
        return;*/
        if (!$helper->checkEmptyLoginOrPassword($login_data, $password_data)) {
            $this->response = array('result' => 0, 'message' => 'Не все поля заполнены!');
            return;
        }

        $contact_model = new docflowContactModel();
        $contact_data = $contact_model->getContact($login_data, $password_data);
        if (empty($contact_data)) {
            $this->response = array('result' => 0, 'message' => "Неверный логин или пароль!");
            return;
        }

        $this->response = array('result' => 1, 'message' => $contact_data[0]);
    }
}