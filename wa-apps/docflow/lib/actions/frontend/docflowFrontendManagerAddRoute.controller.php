<?php

class docflowFrontendManagerAddRouteController extends waJsonController
{
    public function execute()
    {

        $name = waRequest::post('name', null);
        if (empty($name)) {
            $this->response = array('result' => 0, 'message' => "Введите название маршрута");
            return;
        }

        $template_id = waRequest::post('template_id', null);
        if ($template_id == "0") {
            $this->response = array('result' => 0, 'message' => "Выберите шаблон");
            return;
        }

        $data_arr_users_id = array();
        $arr_users_id = waRequest::post('arr_users_id', null);
        foreach ($arr_users_id as $a) {
            if ($a == 0) {
                $this->response = array('result' => 0, 'message' => "Выберите всех пользователей!");
                return;
            }
//            file_put_contents('$arr_id.txt', $a."\n", FILE_APPEND);
        }

        $routes_model = new docflowRoutesModel();
        if ($routes_model->getCountRoute($name)) {
            $this->response = array('result' => 0, 'message' => "Данное название уже существует");
            return;
        }

        $routes_model->addRoute($name, $template_id);
        $route_data = $routes_model->getRouteByName($name);

        foreach ($arr_users_id as $a) {
            array_push($data_arr_users_id, array('id_route' => $route_data[0]['id'], 'id_user' => $a));
        }

        $routes_user_model = new docflowRoutesUsersModel();
        $routes_user_model->addRoutesUsers($data_arr_users_id);

        $this->response = array('result' => 1);

    }
}