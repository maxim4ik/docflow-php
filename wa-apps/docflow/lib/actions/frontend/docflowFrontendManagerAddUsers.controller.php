<?php

class docflowFrontendManagerAddUsersController extends waJsonController
{
    public function execute()
    {

        $login = waRequest::post('login', null);
        if(empty($login))
        {
            $this->response = array('result' => 0, 'message' => "Введите логин");
            return;
        }
        $password = waRequest::post('password', null);
        if(empty($password))
        {
            $this->response = array('result' => 0, 'message' => "Введите пароль");
            return;
        }
        $role_id = waRequest::post('role_id', null);
        if($role_id == 0)
        {
            $this->response = array('result' => 0, 'message' => "Выберите роль");
            return;
        }
        $contact_model = new docflowContactModel();
        if($contact_model->countUser($login))
        {
            $this->response = array('result' => 0, 'message' => "Такой логин уже существует");
            return;
        }

        $contact_model->addUser($login, $password, $role_id);

        $this->response = array('result' => 1);
        //$this->response = array('result' => 1, 'message' => $roles_data);
    }
}