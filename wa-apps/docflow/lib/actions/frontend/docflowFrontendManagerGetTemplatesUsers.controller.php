<?php

class docflowFrontendManagerGetTemplatesUsersController extends waJsonController
{
    public function execute()
    {
        $id = waRequest::post('id', null);

        $templates_roles_model = new docflowTemplatesRolesModel();
        $template_roles_id = $templates_roles_model->getRolesID($id);

        $arr_users = array();
        $contact_model = new docflowContactModel();

        foreach ($template_roles_id as $tri)
        {
            $data_contacts = $contact_model->getTemaplatesContacts($tri['id_role']);
            array_push($arr_users, $data_contacts);
        }

        $this->response = array('arr_users' => $arr_users);

    }
}