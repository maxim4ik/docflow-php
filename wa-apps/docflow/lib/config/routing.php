<?php

return array(
   /* 'loadzip/' => 'frontend/loadzip',
    'loading/' => 'frontend/loading',
    'update/' => 'frontend/update',
    'enter/' => 'frontend/enter',
    'main_menu/' => 'frontend/menu',*/

    'android_login/' => 'frontend/AndroidLogin',
    //'android_get_roles' => 'frontend',

    'android_get_routes/' => 'frontend/AndroidManagerGetRoutes',

    'get_users/' => 'frontend/ManagerGetUsers',
    'add_users/' => 'frontend/ManagerAddUsers',
    'get_roles/' => 'frontend/AdminGetRoles',
    'get_roles_excetp_id/' => 'frontend/AdminGetRolesExceptedId',
    'get_first_roles/' => 'frontend/AdminGetFirstRoles',
    'add_roles/' => 'frontend/AdminAddRoles',
    'get_templates/' => 'frontend/AdminGetTemplates',
    'get_templates_users/' => 'frontend/ManagerGetTemplatesUsers',
    'get_more_template/' => 'frontend/AdminGetTemplate',
    'add_templates/' => 'frontend/AdminAddTemplates',
    'get_routes/' => 'frontend/ManagerGetRoutes',
    'add_route/' => 'frontend/ManagerAddRoute',
    'get_more_route/' => 'frontend/ManagerGetMoreRoute',
    'login/' => 'frontend/login',
    'logout/' => 'frontend/logout',
    'auth/' => 'frontend/auth',
    '' => 'frontend/',
);