<?php
class docflowSession
{
    protected $model;
    protected $session_code;


    public function __construct()
    {
        $this->model = new docflowSessionModel();
        $session_code = waRequest::cookie('sessid', null, 'string');
        $session_data = $this->verifyCode($session_code);
        if (!$session_data) {
            $this->session_code = null;
            wa()->getResponse()->setCookie('sessid', '', time(), null, '', false, true);
        } else {
            $this->session_code = $session_data['session_code'];
        }
    }

    public function isAuth()
    {
        if (!$this->session_code) {
            return false;
        }
        return true;
    }

    public function verifyCode($session_code)
    {
        return $this->model->getById($session_code);
    }

    public function initSession($contact_id)
    {
        //$baggins = waSystem::getInstance('baggins');
        /*$shop_model = new bagginsShopModel();
        if(!$shop_model->getById($shop_id)) {return 'Не удалось получить данные кофейни';}
        */
//        $worker_model = new bagginsWorkerModel();
//        if(!$worker_model->getById($contact_id)) {return 'Не удалось получить данные сотрудника';}

        $this->code = $this->model->createSession($contact_id);
        //$this->shop_id = $shop_id;
        //wa()->getResponse()->setCookie('sessid', $this->code, strtotime('tomorrow'), null, '', false, true);
        wa()->getResponse()->setCookie('sessid', $this->code, time() + 6000, null, '', false, true);
        //return null;
    }

    public function logout()
    {
        //$session_code = waRequest::cookie('sessid', null, 'string');
        wa()->getResponse()->setCookie('sessid', '', time(), null, '', false, true);

    }

}